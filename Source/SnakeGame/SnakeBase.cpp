// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "Interactable.h"


// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bIsMovementAllowed = true;
	ElementSize = 100.f;
	MovementSpeed = 0.5f;
	DefaultLength = 5;
	LastMoveDirection = EMovementDirection::DOWN;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(DefaultLength);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorTickInterval(MovementSpeed);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num()*ElementSize, 0, 0);

		if (SnakeElements.Num() >= DefaultLength)
		{
			auto PrevElement = SnakeElements[SnakeElements.Num()-1];
			NewLocation = PrevElement->GetActorLocation();
		}

		FTransform NewTransform(NewLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	if (bIsMovementAllowed) {
		FVector MovementVector(FVector::ZeroVector);
		FVector Axis = FVector(0.0f, 0.0f, 1.0f);
		float Angle = FMath::DegreesToRadians(-90.0f);
		bool isHorisontal = false;

		bIsMovementAllowed = false;

		switch (LastMoveDirection)
		{
		case EMovementDirection::UP:
			MovementVector.X += ElementSize;
			Angle = FMath::DegreesToRadians(90.0f);
			break;
		case EMovementDirection::DOWN:
			MovementVector.X -= ElementSize;
			Angle = FMath::DegreesToRadians(-90.0f);
			break;
		case EMovementDirection::LEFT:
			MovementVector.Y += ElementSize;
			Angle = FMath::DegreesToRadians(180.0f);
			break;
		case EMovementDirection::RIGHT:
			MovementVector.Y -= ElementSize;
			Angle = FMath::DegreesToRadians(0.0f);
			break;
		}

		SnakeElements[0]->ToggleCollision(); // do i need it? it duplicates at the and of function
		
		for (int i = SnakeElements.Num() - 1; i > 0; i--)
		{
			auto CurrentElement = SnakeElements[i];
			auto PrevElement = SnakeElements[i - 1];
			float BodyRotationValue = 0.0f;

			FVector CurrentLocation = CurrentElement->GetActorLocation();
			FVector PrevLocation = PrevElement->GetActorLocation();

			if (CurrentLocation.X == PrevLocation.X) // find more correct way
			{
				isHorisontal = true;
				if (CurrentLocation.Y > PrevLocation.Y)
				{
					BodyRotationValue = 90;
				}
				else
				{
					BodyRotationValue = -90;
				}
			}
			else if (CurrentLocation.Y == PrevLocation.Y)
			{
				isHorisontal = false;
				if (CurrentLocation.X > PrevLocation.X)
				{
					BodyRotationValue = 0;
				}
				else
				{
					BodyRotationValue = 180;
				}
			}

			FQuat QuatRotation = FQuat(Axis, FMath::DegreesToRadians(BodyRotationValue));
			SnakeElements[i]->SetActorRelativeRotation(QuatRotation);

			if (i == SnakeElements.Num() - 1)
			{
				SnakeElements[i]->SetLastElementType();
			}
			else if (i > 0)
			{
				SnakeElements[i]->SetBasicElementType();
			}

			CurrentElement->SetActorLocation(PrevLocation);
			bIsMovementAllowed = true;
		}
		
		SnakeElements[0]->AddActorWorldOffset(MovementVector);
		FQuat QuatRotation = FQuat(Axis, Angle);
		SnakeElements[0]->SetActorRelativeRotation(QuatRotation);

		SnakeElements[0]->ToggleCollision();
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase * OverlappedElement, AActor* Other)
{
	//�������� �� �������������� � ����������
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

