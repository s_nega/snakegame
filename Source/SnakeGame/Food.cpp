// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
#include "CoreMinimal.h"
#include "Math/UnrealMathUtility.h"
#include "Block.h" 
#include "Kismet/GameplayStatics.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BonusNumber = 0;
	BonusCount = 0;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			FVector NewLocation = GetRandomLocation();
			FVector CurrentLocation = GetActorLocation();
			ABlock* Block = Cast<ABlock>(UGameplayStatics::GetActorOfClass(GetWorld(), ABlock::StaticClass()));

			switch (BonusNumber)
			{
			case 0:// build block 
				if (Block)
				{
					FVector CurrentScale = Block->GetActorRelativeScale3D();
					FVector NewBlockLocation = GetRandomLocation();
					if (Block->BlockScale < 14)
					{
						Block->BlockScale++;
						CurrentScale.X = Block->BlockScale * 0.6f;
					}
					
					Block->SetActorRelativeScale3D(CurrentScale);

					if (Block->BlockScale % 2 == 0)
					{
						NewBlockLocation.X += 30;
					}
				
					if (NewBlockLocation.Y == CurrentLocation.Y || NewBlockLocation.X == CurrentLocation.X) // i had a bug in this block spawn
					{
						if (CurrentLocation.Y >= 240)
						{
							NewBlockLocation.Y = CurrentLocation.Y - 180;
						}
						else
						{
							NewBlockLocation.Y = CurrentLocation.Y + 180;
						}
					}
					Block->SetActorLocation(NewBlockLocation);
				}
				break;
			case 1:// gives many elements to snake
				Snake->AddSnakeElement(5);
				break;
			case 2:// some next food will be in front of current
				if (BonusCount == 0) {
					BonusCount = 5;
				}
				switch (Snake->LastMoveDirection)
				{
				case EMovementDirection::UP:
					NewLocation = CurrentLocation + FVector(60, 0, 0);
					break;
				case EMovementDirection::DOWN:
					NewLocation = CurrentLocation + FVector(-60, 0, 0);
					break;
				case EMovementDirection::LEFT:
					NewLocation = CurrentLocation + FVector(0, 60, 0);
					break;
				case EMovementDirection::RIGHT:
					NewLocation = CurrentLocation + FVector(0, -60, 0);
					break;
				}
				if (NewLocation.X <= -450 || NewLocation.X >= 450 || NewLocation.Y <= -450 || NewLocation.Y >= 450) {
					NewLocation = GetRandomLocation();
				}
				break;
			case 3:// make snake fasrter
				Snake->MovementSpeed -= 0.02;
				break;
			case 4:// make snake slower
				Snake->MovementSpeed += 0.05;
				break;
			}

			int32 RandomNumber = FMath::RandRange(0, 4);

			if (BonusNumber == 2 && BonusCount > 1) {
				BonusNumber = 2;
				BonusCount--;
			}
			else {
				BonusCount = 0;
				BonusNumber = RandomNumber;
				NewLocation = GetRandomLocation();
			}
			SetActorLocation(NewLocation);
		}
	}
}

FVector AFood::GetRandomLocation()
{
	int32 X = FMath::RandRange(-7, 7);
	int32 Y = FMath::RandRange(-7, 7);
	float Z = 0.0f;
	return FVector(X * 60, Y * 60, Z);
}
