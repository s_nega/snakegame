// Fill out your copyright notice in the Description page of Project Settings.


#include "Block.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeBase.h"
//#include "CoreMinimal.h"
//#include "Math/UnrealMathUtility.h"
//#include "Kismet/GameplayStatics.h"

// Sets default values
ABlock::ABlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABlock::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ABlock::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->bIsMovementAllowed = false;
			Snake->Destroy();
		}
	}
}